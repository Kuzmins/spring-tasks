package sda.spring.training.java;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import sda.spring.training.java.beans.*;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.fail;

/*
    Tasks:
    1. Create spring bean
        of type BeanA.java inside SpringJavaConfig.java,
        set string property to 'Hello World!'
        and int property 'to 101'
    2. Remove bean BeanB from SpringJavaConfig.java and observe exception in logs.
    3. Throw IllegalStateException on BeanC inside NonWorkingConfig.java creation or in initialization (inside constructor) and observe exception in tests.
    4. Create 2 beans of Type BeanD with qualifier bean1 and bean2 inside SpringJavaConfig.java.
    5. Inject BeanA into BeanE through constructor or setter inside SpringJavaConfig.java.
    6. Inject 2 beans of type BeanD in BeanF in SpringJavaConfig.java.
*/
class ExplicitBeanDefinitionTest {
    private ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringJavaConfig.class);

    @Test
    public void shouldGetBeanWithCorrectProperty() {
        // When:
        BeanA bean = applicationContext.getBean(BeanA.class);

        // Then:
        assertThat(bean.getStringProperty(), is("Hello World!"));
        assertThat(bean.getIntProperty(), is(101));
    }

    @Test
    public void shouldThrowExceptionIfBeanNotFound() {
        // When:
        NoSuchBeanDefinitionException exception = Assertions.assertThrows(NoSuchBeanDefinitionException.class, () -> {
            applicationContext.getBean(BeanB.class);
        });

        // Then:
        assertThat(exception.getBeanType(), is(BeanB.class));
    }

    @Test
    public void shouldThrowExceptionDuringBeanCreation() {
        // When:
        BeanCreationException exception = Assertions.assertThrows(BeanCreationException.class, () -> {
            new AnnotationConfigApplicationContext(NonWorkingConfig.class);
        });

        // Then:
        assertThat(exception.getBeanName(), is("boomBean"));
    }

    @Test
    public void shouldCreate2BeansOfOneType() {
        // When:
        Map<String, BeanD> beans = applicationContext.getBeansOfType(BeanD.class);

        // Then:
        assertThat(beans.values(), hasSize(2));
        assertThat(beans, hasKey("bean1"));
        assertThat(beans, hasKey("bean2"));
    }

    @Test
    public void shouldGetBeanWithInjectedBean() {
        // When:
        BeanE beanE = applicationContext.getBean(BeanE.class);

        // Then:
        assertThat(beanE.getBeanA(), sameInstance(applicationContext.getBean(BeanA.class)));
    }
    
    @Test
    public void shouldGetBeanWithInjectedBeans() {
        // When:
        BeanF beanE = applicationContext.getBean(BeanF.class);

        // Then:
        assertThat(beanE.getBeanDList(), hasSize(2));
    }
}