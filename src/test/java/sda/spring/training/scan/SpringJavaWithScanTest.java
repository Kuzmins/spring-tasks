package sda.spring.training.scan;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import sda.spring.other.BeanX;
import sda.spring.training.scan.beans.*;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/*
    Tasks:
    1. Add Component scan annotation in SpringJavaWithScanConfig.java.  Make BeanA as Component.
    2. Create 2 Beans BeanB1 and BeanB2 that implement interface InterfaceA. Order them so that first return "A" second "B".
    3. Update BeanC so that BeanA is injected through reflection.
    4. Update BeanD so that BeanA is injected through constructor.
    5 Add `sda.spring.other` package in SpringJavaWithScanConfig.java so That BeanX become available in context.
       Note: If you add package explicitly you have to add also current package which added by default if no other packages declared.
    6. Create explicitly BeanE using @Bean inside SpringJavaWithScanConfig.java and inject BeanA through constructor.
*/
class SpringJavaWithScanTest {
    private final ApplicationContext context = new AnnotationConfigApplicationContext(SpringJavaWithScanConfig.class);

    @Test
    public void shouldGetBeanA() {
        // When:
        BeanA bean = context.getBean(BeanA.class);

        // Then:
        assertThat(bean, is(notNullValue()));
    }

    @Test
    public void shouldGetBeansOfInterfaceAType() {
        // When:
        Map<String, InterfaceA> beans = context.getBeansOfType(InterfaceA.class);

        // Then:
        assertThat(beans.values(), hasSize(2));
    }

    @Test
    public void shouldGetBeanWithInjectedBeanThroughReflection() throws NoSuchFieldException {
        // When:
        BeanC bean = context.getBean(BeanC.class);

        // Then:
        assertThat(bean.getBeanA(), is(sameInstance(context.getBean(BeanA.class))));
        assertThat(BeanC.class.getDeclaredField("beanA").getAnnotations(), arrayWithSize(1));
        assertThat(BeanC.class.getDeclaredField("beanA").getAnnotations()[0].annotationType(), is(Autowired.class));
    }

    @Test
    public void shouldGetBeanWithInjectedBeanThroughConstructor() throws NoSuchMethodException {
        // When:
        BeanD bean = context.getBean(BeanD.class);

        // Then:
        assertThat(bean.getBeanA(), is(sameInstance(context.getBean(BeanA.class))));
        assertThat(BeanD.class.getConstructor(BeanA.class), is(notNullValue()));
    }

    @Test
    public void shouldGetBeanXFromOtherPackage() {
        // When:
        BeanX bean = context.getBean(BeanX.class);

        // Then:
        assertThat(bean, is(notNullValue()));
    }

    @Test
    public void shouldGetBeanEWithBeanAInjected() {
        // When:
        BeanE bean = context.getBean(BeanE.class);

        // Then:
        assertThat(bean, is(notNullValue()));
    }
}