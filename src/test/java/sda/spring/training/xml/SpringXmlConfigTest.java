package sda.spring.training.xml;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import sda.spring.training.java.beans.*;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/*
    Tasks:
    1. Create spring bean of type BeanA.java inside spring-config.xml, set string property to 'Hello world' and int property 'to 101'
    2. Comment bean BeanB from spring-config.xml and observe exception in logs.
    3. Create 2 beans of Type BeanD with qualifier bean1 and bean2 inside spring-config.xml.
    4. Inject BeanA into BeanE through constructor or setter inside spring-config.xml.
    5. Inject 2 beans of type BeanD in BeanE in spring-config.xml.
*/
public class SpringXmlConfigTest {
    private final ApplicationContext applicationContext = new ClassPathXmlApplicationContext("/xml/spring-config.xml");

    @Test
    public void shouldGetBeanWithCorrectProperty() {
        // When:
        BeanA bean = applicationContext.getBean(BeanA.class);

        // Then:
        assertThat(bean.getStringProperty(), is("Hello World!"));
        assertThat(bean.getIntProperty(), is(101));
    }

    @Test
    public void shouldThrowExceptionIfBeanNotFound() {
        // When:
        NoSuchBeanDefinitionException exception = Assertions.assertThrows(NoSuchBeanDefinitionException.class, () -> {
            applicationContext.getBean(BeanB.class);
        });

        // Then:
        assertThat(exception.getBeanType(), is(BeanB.class));
    }

    @Test
    public void shouldCreate2BeansOfOneType() {
        // When:
        Map<String, BeanD> beans = applicationContext.getBeansOfType(BeanD.class);

        // Then:
        assertThat(beans.values(), hasSize(2));
        assertThat(beans, hasKey("bean1"));
        assertThat(beans, hasKey("bean2"));
    }

    @Test
    public void shouldGetBeanWithInjectedBean() {
        // When:
        BeanE beanE = applicationContext.getBean(BeanE.class);

        // Then:
        assertThat(beanE.getBeanA(), sameInstance(applicationContext.getBean(BeanA.class)));
    }

    @Test
    public void shouldGetBeanWithInjectedBeans() {
        // When:
        BeanF beanE = applicationContext.getBean(BeanF.class);

        // Then:
        assertThat(beanE.getBeanDList(), hasSize(2));
    }
}
