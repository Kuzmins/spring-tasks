package sda.spring.training.scan;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import sda.spring.training.scan.beans.BeanA;
import sda.spring.training.scan.beans.BeanE;

@ComponentScan({"sda.spring.other", "sda.spring.training.scan"})
public class SpringJavaWithScanConfig {

    @Bean
    public BeanE beanE(BeanA beanA) {
        return new BeanE(beanA);
    }
}
