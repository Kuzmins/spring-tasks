package sda.spring.training.scan.beans;

public class BeanE {
    private BeanA beanA;

    private String name;

    public BeanE(BeanA beanA) {
        this.beanA = beanA;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
