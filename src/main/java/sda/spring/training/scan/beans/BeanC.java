package sda.spring.training.scan.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanC {
    @Autowired
    private BeanA beanA;

    public BeanA getBeanA() {
        return beanA;
    }
}
