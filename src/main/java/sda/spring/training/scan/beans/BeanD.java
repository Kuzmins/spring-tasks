package sda.spring.training.scan.beans;

import org.springframework.stereotype.Component;

@Component
public class BeanD {
    private BeanA beanA;

    public BeanD(BeanA beanA) {
        this.beanA = beanA;
    }

    public BeanA getBeanA() {
        return beanA;
    }
}
