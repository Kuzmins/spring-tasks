package sda.spring.training.java;

import org.springframework.context.annotation.Bean;
import sda.spring.training.java.beans.BeanA;
import sda.spring.training.java.beans.BeanD;
import sda.spring.training.java.beans.BeanE;
import sda.spring.training.java.beans.BeanF;

import java.util.List;

//1. Create spring bean
//        of type BeanA.java inside SpringJavaConfig.java,
public class SpringJavaConfig {
    @Bean
    public BeanA beanA() {
        BeanA beanA = new BeanA();

        // and int property 'to 101'
        beanA.setIntProperty(101);

        // set string property to 'Hello World!'
        beanA.setStringProperty("Hello World!");

        return beanA;
    }

    @Bean("bean1")
    public BeanD bean1() {
        return new BeanD();
    }

    @Bean("bean2")
    public BeanD bean2() {
        return new BeanD();
    }

    @Bean
    public BeanE beanE(BeanA beanA) {
        return new BeanE(beanA);
    }

    @Bean
    public BeanF beanF(List<BeanD> beanDS) {
        return new BeanF(beanDS);
    }

//    @Bean
//    public BeanB beanB() {
//        return new BeanB();
//    }
}
