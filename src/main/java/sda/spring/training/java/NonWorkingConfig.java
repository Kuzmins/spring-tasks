package sda.spring.training.java;

import org.springframework.context.annotation.Bean;
import sda.spring.training.java.beans.BeanC;

public class NonWorkingConfig {

    @Bean
    public BeanC boomBean() {
        return new BeanC();
    }
}
