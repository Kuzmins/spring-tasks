package sda.spring.training.java.beans;

public class BeanE {
    private final BeanA beanA;

    public BeanE(BeanA beanA) {
        this.beanA = beanA;
    }

    public BeanA getBeanA() {
        return beanA;
    }
}
