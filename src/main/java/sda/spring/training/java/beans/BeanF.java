package sda.spring.training.java.beans;

import java.util.List;

public class BeanF {
    private List<BeanD> beanDList;

    public BeanF(List<BeanD> beanDList) {
        this.beanDList = beanDList;
    }

    public List<BeanD> getBeanDList() {
        return beanDList;
    }
}
