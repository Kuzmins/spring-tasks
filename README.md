## Simple tasks for understatnding bean creation.

There are 3 modules:
* Spring explicit bean creation.
* Automatic bean creation using component scan.
* Bean creation using xml configuration.

Please check these tests:
* ExplicitBeanDefinitionTest.java
* SpringJavaWithScanTest.java
* SpringXmlConfigTest.java

Each test contains list of tasks. You should do them, and run tests. If Everything is correct the tests should pass.

If you want me to check tasks. Please create branch with your name_surname name and create merge request to master.